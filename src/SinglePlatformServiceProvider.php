<?php

namespace Legnd\SinglePlatform;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Legnd\SinglePlatform\Api\PublisherApi;

class SinglePlatformServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/singleplatform.php' => config_path('singleplatform.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__ . '/../config/singleplatform.php', 'singleplatform'
        );

        $this->app->bind(SinglePlatform::class, function ($app) {

            $config = [
                'token' => $app['config']->get('singleplatform.token'),
                'secret' => $app['config']->get('singleplatform.secret')
            ];

            $id = $app['config']->get('singleplatform.locationId');

            return new SinglePlatform($id, new PublisherApi(new Client([
                'base_uri' => PublisherApi::$baseUrl
            ]), $config));

        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [SinglePlatform::class];
    }

}
