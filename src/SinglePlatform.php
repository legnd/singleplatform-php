<?php

namespace Legnd\SinglePlatform;

use Legnd\SinglePlatform\Api\Resources\Menu;

class SinglePlatform
{
    protected $api, $id;

    public function __construct($id, $api)
    {
        $this->id = $id;
        $this->api = $api;
    }

    public function getLocationId()
    {
        return $this->id;
    }

    public function setLocationId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Upon success, returns a sorted Collection of Menu resources for the current location.
     *
     * @return mixed
     * @throws \Exception
     */
    public function getMenusCollection()
    {
        if ($this->id) {
            return collect($this->api->getMenus($this->id))
                ->sortBy('order_num')
                ->mapInto(Menu::class);
        }

        throw new \Exception("Invalid or missing location ID.");
    }

    /**
     * Upon success, returns all results of the sorted Menus
     *
     * @return mixed
     * @throws \Exception
     */
    public function getMenus()
    {
        return $this->getMenusCollection()->all();
    }

    /**
     * Upon success, returns a single Menu matching the ID
     *
     * @param int $menuId
     * @return mixed
     * @throws \Exception
     */
    public function getMenuById($menuId)
    {
        return $this->getMenusCollection()->filter(function($menu) use($menuId){
            return $menu->id === (int)$menuId;
        })->first();
    }
}