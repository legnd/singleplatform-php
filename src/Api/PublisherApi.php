<?php

namespace Legnd\SinglePlatform\Api;

use GuzzleHttp\TransferStats;

/**
 * Class PublisherApi
 * @package Legnd\SinglePlatform\Api
 */
class PublisherApi
{

    static public $baseUrl = 'http://publishing-api.singleplatform.com';

    protected $client;
    protected $token;
    protected $secret;

    /**
     * PublisherApi constructor.
     * @param $client
     * @param $config
     */
    public function __construct($client, $config)
    {
        $this->setClient($client);
        $this->setConfig($config);
    }

    /**
     * Return the menus for a location
     *
     * @param $locationId
     * @return mixed
     * @throws \HttpResponseException
     */
    public function getLocation($locationId)
    {
        $uri = "/locations/$locationId/";
        $response = $this->client()->get($uri, $this->signRequestOptions($uri));

        return $this->handleResponse($response);
    }

    /**
     * Return the menus for a location
     *
     * @param $locationId
     * @return mixed
     * @throws \HttpResponseException
     */
    public function getMenus($locationId)
    {
        $uri = "/locations/$locationId/menus/";
        $response = $this->client()->get($uri, $this->signRequestOptions($uri));

        return $this->handleResponse($response);
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        if (!empty($config['token']) && !empty($config['secret'])) {
            $this->token = $config['token'];
            $this->secret = $config['secret'];
        }
    }

    /**
     * @param $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function client()
    {
        return $this->client;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return !empty($this->token) && !empty($this->secret);
    }

    /**
     * @return mixed
     */
    protected function getClientToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    protected function getClientSecret()
    {
        return $this->secret;
    }

    /**
     * Instantiate our Request Options array
     *
     * @param array $options
     * @return array
     */
    protected function getRequestOptions($options = [])
    {
        // Merge client token every time
        return array_merge_recursive(
            $options,
            ['query' => ['client' => $this->getClientToken()]]
        );
    }

    /**
     * Instantiate our Request Options array and sign the request
     *
     * @param string $uri
     * @param array $options
     * @return array
     */
    protected function signRequestOptions($uri, $options = [])
    {
        $options = $this->getRequestOptions($options);
        $fullUri = $uri . "?" . http_build_query($options['query']);
        $signed  = base64_encode(hash_hmac('sha1', $fullUri, $this->getClientSecret(), true));
        $options['query']['signature'] = $signed;

        return $options;
    }

    /**
     * Check that the request returns an "OK" successful response.
     *
     * @param $response
     * @return mixed
     * @throws \HttpResponseException
     */
    public function handleResponse($response)
    {
        if ($response->getStatusCode() == "200") {
            return json_decode($response->getBody())->data;
        }

        throw new \HttpResponseException("An error occurred during the request.");
    }

}