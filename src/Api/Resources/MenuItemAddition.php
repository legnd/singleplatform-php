<?php

namespace Legnd\SinglePlatform\Api\Resources;

class MenuItemAddition extends AbstractResource
{
    public function getPriceValue()
    {
        return object_get($this->data, 'prices.min');
    }
}