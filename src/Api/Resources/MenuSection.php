<?php

namespace Legnd\SinglePlatform\Api\Resources;

class MenuSection extends AbstractResource
{

    public function items()
    {
        return $this->get('items')->sortBy('order_num')->mapInto(MenuItem::class)->all();
    }

}