<?php

namespace Legnd\SinglePlatform\Api\Resources;

class MenuItem extends AbstractResource
{
    public function getPriceValue()
    {
        $firstChoice = $this->get('choices')[0] ?? null;
        if ($firstChoice) {
            return object_get($firstChoice, 'prices.min');
        }
        return null;
    }

    public function additions()
    {
        return $this->get('additions')->sortBy('order_num')->mapInto(MenuItemAddition::class)->all();
    }
}