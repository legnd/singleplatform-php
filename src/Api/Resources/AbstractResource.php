<?php

namespace Legnd\SinglePlatform\Api\Resources;

abstract class AbstractResource
{

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Gets the data key from the resource. Dot notation supported.
     * Returns the value or a Collection if the value is an array.
     *
     * @param $dottedKey
     * @return \Illuminate\Support\Collection|mixed
     */
    public function get($dottedKey)
    {
        $val = object_get($this->data, $dottedKey);
        return is_array($val)
            ? collect($val)
            : $val;
    }

    /**
     * Gets a data key, or if a mutator method exists, returns the value
     * of the mutator method. Mutators must follow the getFieldValue
     * format where Field is the studly case version of the requested key.
     *
     * For an example use case see getPriceValue method in MenuItem class.
     *
     * @param $dottedKey
     * @return \Illuminate\Support\Collection|mixed
     */
    public function __get($dottedKey)
    {
        $mutator = "get" . studly_case($dottedKey) . "Value";
        return method_exists($this, $mutator)
            ? $this->$mutator()
            : $this->get($dottedKey);
    }

}