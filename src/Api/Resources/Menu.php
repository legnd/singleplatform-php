<?php

namespace Legnd\SinglePlatform\Api\Resources;

class Menu extends AbstractResource
{
    public function sectionsCollection()
    {
        return $this->get('sections')
                    ->sortBy('order_num')
                    ->mapInto(MenuSection::class)
            ;
    }

    public function sections()
    {
        return $this->sectionsCollection()->all();
    }

    public function getSectionById($id)
    {
        return $this->sectionsCollection()
                    ->filter(function ($section) use ($id) {
                        return $section->id === (int)$id;
                    })
                    ->first()
            ;
    }
}