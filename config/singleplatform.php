<?php
return [
    'token' => env('SINGLEPLATFORM_KEY'),
    'secret' => env('SINGLEPLATFORM_SECRET'),
    'locationId' => env('SINGLEPLATFORM_LOCATION'),
];