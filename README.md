# Installation

## Require via composer
`composer require legnd/singleplatform-php`

# Usage

## Simple PHP Example 
```php
require 'vendor/autoload.php';
use \Legnd\SinglePlatform\Api\PublisherApi;
use \Legnd\SinglePlatform\SinglePlatform;

$id     = 'my-restaurant';
$config = [
    'token'  => 'YOUR_CLIENT_KEY', // Note: this is the Client Key not the API token
    'secret' => 'YOUR_CLIENT_SECRET'
];

$api = new PublisherApi(new GuzzleHttp\Client([
   'base_url' => PublisherApi::$baseUrl
]), $config);

$service = new SinglePlatform($id, $api);

echo $service->getMenus()[0]->sections()[0]->items()[0]->price;
```

## Laravel (via Service Provider)

### Package Discovery
The service provider will be auto-discovered by Laravel via Composer

### Publish config (optional)
`php artisan vendor:publish`

### Add the following `.env` variables
```
SINGLEPLATFORM_KEY=YOUR_CLIENT_KEY
SINGLEPLATFORM_SECRET=YOUR_CLIENT_SECRET
SINGLEPLATFORM_LOCATION=YOUR_RESTAURANT_ALPHAID
```

### Use in a controller

```php
use Facades\Legnd\SinglePlatform\SinglePlatform;

class RestaurantController {

    public function menus()
    {
        return SinglePlatform::getMenus();
    }

}
```